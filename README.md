Часть на Angular не делал, возможно когда-нибудь напишу для себя(а надо ли?:))
Текст оригинального задания:

1. Изучить/понять механизм Deferred/Promise

 2. Необходимо сделать одностраничное приложение, реализующее CRUD операции по этим сущностям:

 - Person (person_id, first_name, last_name, middle_name, email, phone_number);
 - User (user_id, nickname, department_id, person_id, position_id, super_user);
 - Position (position_id, position_name, salary);
 - Department (department_id, department_name, company_id);
 - Company (company_id, company_name, description, logo).

 Редактирование производить, к примеру, в Popup окне.

 Условия:
 - данные брать из JSON.file или использовать LocalStorage
 - не использовать js-фреймворки!
 - только html, css, javascript, jQuery
 - красиво, функционально и интуитивно понятно - это важно
 - для эмуляции отправки данных resolve - 1.5 секунды
 - можно использовать css-фреймворки bootstrap, foundation и т.д.
 - сделать notifications, используя, например, jGrowl

 3. Все тоже самое, что и во втором задании (под цифрой 2), только нужно реализовать с использованием Angular.js

 Результатом будет являться репозиторий на Bitbucket.
 ! Обязательно наличие истории разработки.