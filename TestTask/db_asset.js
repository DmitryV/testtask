﻿function supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
};

function pushDataBase() {
    if (!supports_html5_storage()) {
        return;
    }
    if (!localStorage['Person']) {
        localStorage['Person'] = JSON.stringify([{
            person_id: 1,
            first_name: 'Person 1',
            last_name: 'LastName1',
            middle_name: 'MidName1',
            email: 'example1@mail.com',
            phone_number: '+3(123)4567890'
        }, {
            person_id: 2,
            first_name: 'Person 2',
            last_name: 'LastName2',
            middle_name: 'MidName2',
            email: 'example2@mail.com',
            phone_number: '+4(321)6567860'
        }
        ]);
    }
    if (!localStorage['User']) {
        localStorage['User'] = JSON.stringify([{
            user_id: 1,
            nickname: 'nickname1',
            department_id: 1,
            person_id: 1,
            position_id: 1,
            super_user: 2
        }, {
            user_id: 2,
            nickname: 'nickname2',
            department_id: 1,
            person_id: 2,
            position_id: 2,
            super_user: 2
        }]);
    }
    if (!localStorage['Position']) {
        localStorage['Position'] = JSON.stringify([
            { position_id: 1, position_name: 'position1', salary: 999.99 },
            { position_id: 2, position_name: 'position2', salary: 1999.99 }
        ]);
    }
    if (!localStorage['Department']) {
        localStorage['Department'] = JSON.stringify([{
            department_id: 1, department_name: 'department1', company_id: 1
        }]);
    }
    if (!localStorage['Company']) {
        localStorage['Company'] = JSON.stringify([{
            company_id: 1, company_name: 'company1', description: 'a large company', logo: "https://assets.onestore.ms/cdnfiles/onestorerolling-1510-07012/shell/v2_5/images/logo/microsoft.png"
        }]);
    }
};